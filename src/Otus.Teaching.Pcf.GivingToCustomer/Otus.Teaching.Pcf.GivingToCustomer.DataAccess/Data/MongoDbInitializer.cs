﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IMongoCollection<Preference> preferenceMongoCollection;
        private readonly IMongoCollection<Customer> customerMongoCollection;
        private readonly IMongoCollection<PromoCode> promoCodeMongoCollection;

        public MongoDbInitializer(
            IMongoCollection<Preference> preferenceMongoCollection,
            IMongoCollection<Customer> customerMongoCollection,
            IMongoCollection<PromoCode> promoCodeMongoCollection)
        {
            this.preferenceMongoCollection = preferenceMongoCollection;
            this.customerMongoCollection = customerMongoCollection;
            this.promoCodeMongoCollection = promoCodeMongoCollection;
        }
        public void InitializeDb()
        {
            preferenceMongoCollection.DeleteMany(x => true);
            customerMongoCollection.DeleteMany(x => true);
            promoCodeMongoCollection.DeleteMany(x => true);

            preferenceMongoCollection.InsertMany(FakeDataFactory.Preferences);
            customerMongoCollection.InsertMany(FakeDataFactory.Customers);
        }
    }
}
