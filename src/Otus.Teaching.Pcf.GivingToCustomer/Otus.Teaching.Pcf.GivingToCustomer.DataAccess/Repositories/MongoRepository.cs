﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class MongoRepository<T>
        : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoCollection<T> mongoCollection;

        public MongoRepository(IMongoCollection<T> mongoCollection)
        {
            this.mongoCollection = mongoCollection;
        }

        public async Task AddAsync(T entity)
        {
            await mongoCollection
                .InsertOneAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await mongoCollection
                .DeleteOneAsync(item => item.Id == entity.Id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var cursor = await mongoCollection
                .FindAsync(FilterDefinition<T>.Empty);
            return await cursor.ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var cursor = await mongoCollection
                .FindAsync(e => e.Id == id);
            return await cursor
                .FirstOrDefaultAsync();
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            var cursor = await mongoCollection
                .FindAsync(predicate);
            return await cursor.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var range = await mongoCollection
                .FindAsync(e => ids.Contains(e.Id));
            return await range
                .ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            var cursor = await mongoCollection.FindAsync(predicate);
            return await cursor.ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            await mongoCollection
                .ReplaceOneAsync(item => item.Id == entity.Id, entity);
        }
    }
}
