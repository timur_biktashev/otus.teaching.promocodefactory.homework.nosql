﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IMongoCollection<Employee> employeesMongoCollection;
        private readonly IMongoCollection<Role> rolesMongoCollection;

        public MongoDbInitializer(
            IMongoCollection<Employee> employeesMongoCollection,
            IMongoCollection<Role> rolesMongoCollection)
        {
            this.employeesMongoCollection = employeesMongoCollection;
            this.rolesMongoCollection = rolesMongoCollection;
        }
        public void InitializeDb()
        {
            employeesMongoCollection.DeleteMany(x => true);
            rolesMongoCollection.DeleteMany(x => true);

            employeesMongoCollection.InsertMany(FakeDataFactory.Employees);
            rolesMongoCollection.InsertMany(FakeDataFactory.Roles);
        }
    }
}
